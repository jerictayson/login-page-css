
const USERNAME = 'admin';
const PASSWORD = 'password';
const btnLogin = document.getElementById('btnSubmit');
let message = document.getElementById('status');
let userInput = document.getElementsByTagName('input');

for(var i=0; i<userInput.length; i++){

    userInput[i].addEventListener('keydown', function(){
        message.style.visibility = 'hidden';
    });

}

btnLogin.addEventListener('click', checkLogin);
function checkLogin(e){

    e.preventDefault();
    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    if(!username || !password){

        message.innerText = "Input cannot be empty.";
        
    }else {

        if(username === USERNAME && password === PASSWORD){

            for(var i=0; i<userInput.length; i++){

                userInput[i].removeEventListener('keydown', function(){
                message.style.visibility = 'hidden';
                });
            }

            message.innerText = "Successfully Logged In!";
        }else {


        message.innerText = "Invalid Username/Password.";
        }
    }
    
    message.style.visibility = 'visible';
}